package com.eniro.manyana

import java.net.InetSocketAddress
import java.nio.charset.{Charset, StandardCharsets}

import zio.{App, ZIO, Task, ExitCode}
import zio.blocking._

import com.typesafe.scalalogging.StrictLogging
import scala.util.Properties.envOrNone

import uzhttp.server.Server
import uzhttp.{Request, Response, RefineOps, Status}
import uzhttp.websocket.Frame
import com.eniro.manyana.nats.NatsService

object Main extends App with StrictLogging {
  val successJson = """{"status":"sucess"}"""
  val logError: String => Task[Unit] = msg => Task.effect(logger.error(msg))

  // stancli -url nats://172.17.0.2:4222 -cluster test-cluster -subject subject1 message.json
  override def run(args: List[String]): ZIO[zio.ZEnv, Nothing, ExitCode] =
    Server
      .builder(new InetSocketAddress("0.0.0.0", config.port.number))
      .handleSome {
        case req if req.uri.getPath == "/start" =>
          effectBlocking(NatsService.service())
            .catchAll(t =>
              logError(s"${t.getMessage()}\n${t.getStackTrace.mkString("\n")}")
            )
            .fork *> ZIO.succeed(json(successJson))
        case req if req.uri.getPath() == "/stop" =>
          Task.succeed(NatsService.stop()) *> ZIO.succeed(json(successJson))
      }
      .serve
      .useForever
      .orDie

  def json(
      body: String,
      status: Status = Status.Ok,
      headers: List[(String, String)] = Nil,
      charset: Charset = StandardCharsets.UTF_8
  ): Response =
    Response.const(
      body.getBytes(charset),
      status,
      contentType = s"application/json; charset=${charset.name()}",
      headers = headers
    )
}
