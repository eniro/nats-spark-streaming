package com.eniro.manyana.nats

import java.time.Instant
import java.time.temporal.ChronoUnit

import com.typesafe.scalalogging.StrictLogging
import com.eniro.manyana.config

import com.logimethods.connector.nats.to_spark.NatsToSparkConnector

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import org.apache.spark.SparkContext
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.{Seconds, StreamingContext}

import com.datastax.spark.connector._
import org.apache.spark.sql.cassandra._

object NatsService extends StrictLogging {

  private val createSparkconf: () => SparkConf = () => {
    val sparkConf = new SparkConf()
      .set("spark.cassandra.connection.host", config.cassandra.server)
      .set("spark.cassandra.auth.username", config.cassandra.user)
      .set("spark.cassandra.auth.password", config.cassandra.password)
      .set(
        "spark.cassandra.connection.keepAliveMS",
        config.cassandra.timeout.toString
      )
      .set("spark.metrics.namespace", config.spark.metricsNs)
      .set("spark.cassandra.output.consistency.level", "LOCAL_QUORUM")
      .set("spark.sql.session.timeZone", "UTC")

    if (config.bucket.server.isDefined) {
      logger.info(">>>> Minio Defined")
      sparkConf
        .set("spark.hadoop.fs.s3a.endpoint", config.bucket.server.get)
        .set("spark.hadoop.fs.s3a.access.key", config.bucket.accessKey)
        .set("spark.hadoop.fs.s3a.secret.key", config.bucket.secretKey)
        .set(
          "spark.hadoop.fs.s3a.aws.credentials.provider",
          "org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider"
        )
        .set("spark.hadoop.fs.s3a.path.style.access", "false")
        .set("spark.hadoop.fs.s3a.connection.ssl.enabled", "true")
        .set(
          "spark.hadoop.fs.s3a.impl",
          "org.apache.hadoop.fs.s3a.S3AFileSystem"
        )
    }

    sparkConf
  }

  private def getConf(): SparkConf = createSparkconf().setMaster(config.spark.master)

  private val appName = "NatsSpark"

  def service(): Unit = {

    val checkpoint = config.spark.checkpoint.getOrElse("/tmp")
    val sparkConf = getConf().setAppName(appName)

    logger.info(s"master: ${config.spark.master}")
    logger.info(
      s"Subject: ${config.nats.subject} Start Instant: ${config.nats.startInstant}"
    )

    val sc = SparkContext.getOrCreate(sparkConf)
    val ssc =
      new StreamingContext(sc, Seconds(config.spark.batchDuration))
    ssc.checkpoint(checkpoint)

    val clusterID = config.nats.clusterId
    val start =
      Instant.now.minus(config.nats.startInstant, ChronoUnit.MINUTES)

    val messages: ReceiverInputDStream[String] = NatsToSparkConnector
      .receiveFromNatsStreaming(
        classOf[String],
        StorageLevel.MEMORY_ONLY_SER,
        clusterID
      )
      .withNatsURL(config.nats.stanUrl)
      .withSubjects(config.nats.subject)
      .durableName(config.nats.durableName)
      .startAtTime(start)
      .asStreamOf(ssc)

    messages.foreachRDD(rdd => {
      rdd.checkpoint()
      rdd
        .flatMap(MessageProcessor.process)
        .saveToCassandra(
          config.cassandra.keyspace,
          "conversions_monitor",
          SomeColumns("ecoid", "service", "change", "at", "status", "content")
        )
    })

    ssc.start()
    ssc.awaitTermination()
  }

  def stop(): Unit = {
    StreamingContext.getActive().flatMap(ssc => Some(ssc.stop(true, true)))
  }
}
