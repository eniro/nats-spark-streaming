package com.eniro.manyana.nats

import com.typesafe.scalalogging.StrictLogging
import io.circe.optics.{JsonFoldPath, JsonPath}
import io.circe.{Json, JsonLong, JsonNumber, JsonObject}
import io.circe.optics.JsonPath.root
import io.circe.parser.parse
import monocle.{Optional, Traversal}

import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import com.eniro.manyana._

case object MessageProcessor extends StrictLogging {

  private def parseMessage(msg: String): Json =
    parse(msg) match {
      case Right(json) => json
      case Left(failure) => {
        logger.error(s"${failure} - $msg")
        Json.Null
      }
    }

  private val ecoIdL: Optional[Json, JsonNumber] = root.State.ecoId.number
  private val targetsL: Optional[Json, JsonObject] = root.State.targets.obj
  private val updatedAtL: Optional[Json, String] = root.updatedAt.string
  private val statusL: Optional[Json, String] = root.status.string
  private val contentL: Optional[Json, Json] = root.content.json
  private val servicesL: Optional[Json, JsonObject] = root.State.targets.obj

  private def encodeTarget(service: String): String =
    service match {
      case "eniro"       => "en"
      case "degulesider" => "gs"
      case "dgs"         => "dg"
      case "krak"        => "kr"
      case _ => {
        logger.warn(s"Undefined service found: $service")
        "undefined"
      }
    }

  def process(
      msg: String
  ): List[(EcoId, Service, Change, Stamp, CYBStatus, Content)] = {
    val json: Json = parseMessage(msg)
    val ecoId: String =
      ecoIdL.getOption(json).map(_.toString()).getOrElse("UNDEFINED")
    val targets: Map[String, Json] = targetsL
      .getOption(json)
      .map(j => j.toMap)
      .getOrElse(Map.empty[String, Json])

    targets
      .foldLeft(
        List.empty[(EcoId, Service, Change, Stamp, CYBStatus, Content)]
      )((acc, cur) => {
        val service = encodeTarget(cur._1)
        val fieldMap: Map[String, Json] =
          cur._2.asObject.map(_.toMap).getOrElse(Map.empty[String, Json])

        fieldMap
          .filter(t => updatedAtL.getOption(t._2).isDefined)
          .foldLeft(
            List.empty[(EcoId, Service, Change, Stamp, CYBStatus, Content)]
          )((acc1, c) => {
            val change = c._1
            val status = statusL.getOption(c._2).getOrElse("MODIFIED")
            val content = contentL.getOption(c._2).fold("")(_.noSpaces)

            val at =
              updatedAtL.getOption(c._2).getOrElse("2017-01-01T00:00:00.000Z")
            val atZDT = ZonedDateTime.parse(at, DateTimeFormatter.ISO_DATE_TIME)
            val aMonthAgo = LocalDateTime.now.minusMonths(config.nats.monthsThreshold)

            if (aMonthAgo.isBefore(atZDT.toLocalDateTime())) {
              val ret = (ecoId, service, change, at, status, content)
              ret +: acc1
            } else acc1
          }) ++ acc
      })
  }
}
