package com.eniro

import org.apache.spark.sql.{Column, DataFrame, Dataset, Row, SaveMode, functions => f}
import org.apache.spark.sql.cassandra._
import org.apache.spark.rdd.RDD
import com.typesafe.scalalogging.StrictLogging

package object manyana extends StrictLogging {

  val config: AppConfig = AppSetup.conf()

  type EcoId = String
  type Service = String
  type Change = String
  type CYBStatus = String
  type Content = String
  type Stamp = String

}
