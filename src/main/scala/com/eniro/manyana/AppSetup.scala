package com.eniro.manyana

import com.typesafe.scalalogging.StrictLogging

import pureconfig._
import pureconfig.generic.auto._

import org.apache.spark.SparkConf

case class Env(name: String) extends AnyVal

case class Port(number: Int) extends AnyVal

case class SparkConfig(
    master: String,
    checkpoint: Option[String],
    batchDuration: Long,
    metricsNs: String
)

case class CassandraConfig(
    server: String,
    user: String,
    password: String,
    keyspace: String,
    timeout: Int,
    throughput: Int
)

case class NatsConfig(
    stanUrl: String,
    subject: String,
    clusterId: String,
    startInstant: Long,
    durableName: String,
    monthsThreshold: Int
)

case class BucketConfig(
    server: Option[String],
    accessKey: String,
    secretKey: String
)

case class AppConfig(
    env: Env,
    port: Port,
    spark: SparkConfig,
    cassandra: CassandraConfig,
    nats: NatsConfig,
    bucket: BucketConfig
)

object AppSetup extends StrictLogging {

  val defaultSparkConfig =
    SparkConfig("", None, 5000, "nats_streaming")
  val defaultCassConfig = CassandraConfig("", "", "", "", 5000, 5)
  val defaultNatsConfig = NatsConfig(
    "nats://localhost:4222",
    "subject1",
    "test-cluster",
    10L,
    "durable_subscriber",
    1
  )
  val defaultBucketConfig = BucketConfig(None, "", "") //not used on local

  def conf(): AppConfig = {
    ConfigSource.default.load[AppConfig] match {
      case Right(appConf) => appConf
      case Left(failures) => {
        failures.toList.foreach(f => logger.error(f.toString()))
        AppConfig(
          Env("dev"),
          Port(8080),
          defaultSparkConfig,
          defaultCassConfig,
          defaultNatsConfig,
          defaultBucketConfig
        )
      }
    }
  }
}
