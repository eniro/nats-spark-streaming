package com.eniro.manyana.nats

import org.scalatest.FlatSpec

import scala.io.Source

class MessageProcessorTest extends FlatSpec {
  "process" should "parse json and extract timestamp, ecoId, status and content" in {
    val messagesSource = Source
      .fromInputStream(
        this.getClass.getResourceAsStream("/message.json")
      )
      .mkString

    val results = MessageProcessor.process(messagesSource)

    assert(results.size == 2)

    val expected1 = """{"name":"asdasasd"}"""
    assert(results(1)._1 == "16302553")
    assert(results(1)._2 == "en")
    assert(results(1)._3 == "name")
    assert(results(1)._4 == "2020-09-30T08:51:26.509Z")
    assert(results(1)._5 == "CHANGED")
    assert(results(1)._6 == expected1)

    val expected2 = """{"phoneNumber":"555-555555","Country":"SE"}"""
    assert(results(0)._1 == "16302553")
    assert(results(0)._2 == "en")
    assert(results(0)._3 == "phoneNumber")
    assert(results(0)._4 == "2021-04-02T08:51:26.509Z")
    assert(results(0)._5 == "CHANGED")
    assert(results(0)._6 == expected2)
  }

  "process" should "parse json from different targets on the same message" in {
    val messagesSource = Source
      .fromInputStream(
        this.getClass.getResourceAsStream("/weird_description.json")
      )
      .mkString

    val results = MessageProcessor.process(messagesSource)

    assert(results.size == 2)

    val expected1 = """{"description":"heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen\n\nheja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen\n\nheja bajen heja bajen heja bajen heja bajen heja bajen heja bajen h åäö"}"""
    assert(results(0)._1 == "141607456")
    assert(results(0)._2 == "kr")
    assert(results(0)._3 == "description")
    assert(results(0)._4 == "2020-11-23T09:04:15.464Z")
    assert(results(0)._5 == "ADDED")
    assert(results(0)._6 == expected1)

    val expected2 = """{"description":"heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen\n\nheja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen heja bajen\n\nheja bajen heja bajen heja bajen heja bajen heja bajen heja bajen h åäö"}"""
    assert(results(1)._1 == "141607456")
    assert(results(1)._2 == "dg")
    assert(results(1)._3 == "description")
    assert(results(1)._4 == "2020-11-23T09:04:15.464Z")
    assert(results(1)._5 == "ADDED")
    assert(results(1)._6 == expected2)

  }

}
