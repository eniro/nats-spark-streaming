import sbt._

object Dependencies {

  val sparkVersion = "3.0.1"
  val sparkTestBaseVersion = "2.4.3_0.12.0"
  val natsVersion = "1.0.0"
  val circeVersion = "0.13.0"
  val zioVersion = "1.0.1"
  val interopsVersion = "2.1.4.0"
  val hadoopVersion = "2.10.1"

  final val SPARK_ENV = "ENV"
  final val APP_ARGS = "APP_ARGS"

  val envStr: String = sys.env.get(APP_ARGS) match {
    case Some(v) => v
    case None    => sys.env.getOrElse(SPARK_ENV, "dev")
  }

  val spark_common = Seq(
    "org.apache.spark" %% "spark-streaming-kafka-0-10" % sparkVersion,
    "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion,
    "com.datastax.spark" % "spark-cassandra-connector_2.12" % "3.0.1",
    "joda-time" % "joda-time" % "2.10.10"
  )

  val spark_local = Seq(
    "org.apache.spark" %% "spark-streaming" % sparkVersion,
    "org.apache.spark" %% "spark-core" % sparkVersion,
    "org.apache.spark" %% "spark-sql" % sparkVersion,
    "org.apache.hadoop" % "hadoop-aws" % hadoopVersion,
    "org.apache.hadoop" % "hadoop-common" % hadoopVersion,
    "com.logimethods" % "nats-connector-spark" % natsVersion exclude("org.apache.spark", "spark-streaming_2.11")
  )

  val spark_non_local = Seq(
    "org.apache.spark" %% "spark-streaming" % sparkVersion % "provided",
    "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
    "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
    "org.apache.hadoop" % "hadoop-aws" % hadoopVersion % "provided",
    "org.apache.hadoop" % "hadoop-common" % hadoopVersion % "provided",
    "com.logimethods" % "nats-connector-spark" % natsVersion exclude("org.apache.spark", "spark-streaming_2.11")
  )

  val spark = envStr match {
    case "dev" | "local" => spark_local ++ spark_common
    case _     => spark_non_local ++ spark_common
  }

  val logging = Seq(
    "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2"
  )

  val configParser = Seq(
    "com.github.pureconfig" %% "pureconfig" % "0.14.1"
  )

  val zio = Seq(
    "dev.zio" %% "zio" % zioVersion,
    "dev.zio" %% "zio-interop-cats" % interopsVersion
  )

  val uzhttp = Seq(
    "org.polynote" %% "uzhttp" % "0.2.5"
  )

  val circe = Seq(
    "io.circe" %% "circe-core" % circeVersion,
    "io.circe" %% "circe-generic" % circeVersion,
    "io.circe" %% "circe-parser" % circeVersion,
    "io.circe" %% "circe-optics" % circeVersion,
    "org.typelevel" %% "cats-core" % "2.1.1"
  )

  val misc = Seq(
    "commons-validator" % "commons-validator" % "1.6",
    "commons-configuration" % "commons-configuration" % "1.10",
    "com.lihaoyi" %% "os-lib" % "0.6.2",
    //tests
    "org.scalactic" %% "scalactic" % "3.0.3" % "test",
    "com.holdenkarau" %% "spark-testing-base" % sparkTestBaseVersion % "test",
    "com.github.mrpowers" %% "spark-fast-tests" % "0.23.0" % "test"
  )
}
