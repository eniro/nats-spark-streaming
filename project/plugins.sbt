logLevel := Level.Warn

resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

//addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.0")
//addSbtPlugin("org.wartremover" % "sbt-wartremover" % "2.4.2")
//addSbtPlugin("com.geirsson" % "sbt-scalafmt" % "1.5.1")
addSbtPlugin("org.xerial.sbt" % "sbt-sonatype" % "2.0")
addSbtPlugin("com.jsuereth" % "sbt-pgp" % "1.1.0-M1")
